/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada.controller;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.jwork.dhammapada.ChapterFragment;
import com.jwork.dhammapada.Constant;
import com.jwork.dhammapada.Controller;
import com.jwork.dhammapada.R;
import com.jwork.dhammapada.SearchFragment;
import com.jwork.dhammapada.VerseFragment;
import com.jwork.dhammapada.model.Chapter;
import com.jwork.dhammapada.model.SearchVerse;
import com.jwork.dhammapada.model.Verse;
import com.jwork.dhammapada.utility.Configuration;
import com.jwork.dhammapada.utility.CrashHandler;

public class DhammapadaController extends Controller {
	
	public DhammapadaController(FragmentActivity activity, Handler viewHandler) {
		super(activity, viewHandler);
	}

	@Override
	public void handleMessage(Message msg) {
		log.v(this, "handleMessage("+msg.what+")");
		switch (msg.what) {
		case Constant.WHAT_DISPLAY_CHAPTER:
			displayChapter();
			break;
		case Constant.WHAT_SELECT_CHAPTER:
			selectChapter((Chapter)msg.obj);
			break;
		case Constant.WHAT_INIT:
			init();
			break;
		case Constant.WHAT_SHARE_VERSE:
			shareVerse((Verse)msg.obj);
			break;
		case Constant.WHAT_GOTO_BACK:
			gotoPreviousFragment((Fragment)msg.obj);
			break;
		case Constant.WHAT_SEARCH_VERSE:
			searchVerse((CharSequence)msg.obj);
			break;
		case Constant.WHAT_SELECT_VERSE:
			selectVerse((SearchVerse)msg.obj);
			break;
		}
	}

	private void searchVerse(CharSequence query) {
		log.v(this, "selectVerse("+query+")");
		FragmentManager fm = activity.getSupportFragmentManager();
		fm.beginTransaction()
		.replace(R.id.contentFragment, SearchFragment.newInstance(query.toString()), VerseFragment.class.getName())
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			.addToBackStack(VerseFragment.class.getName())
			.commit();
	}

	private void gotoPreviousFragment(Fragment current) {
		log.v(this, "gotoPreviousFragment()");
		FragmentManager fm = activity.getSupportFragmentManager();
		fm.popBackStack();
	}

	private void shareVerse(Verse verse) {
		Intent intentText = new Intent(Intent.ACTION_SEND);
		intentText.setType("text/plain");
		intentText.putExtra(Intent.EXTRA_SUBJECT, verse.getChapter().getTitlePali() + " - " + verse.getIndex()); 
		intentText.putExtra(Intent.EXTRA_TEXT, verse.getContentPali()+"\n\n"+verse.getContentEng()+"\n\n"+verse.getContentId());
		try {
			activity.startActivity(Intent.createChooser(intentText, "Select your favorite Social Media network"));
		} catch (ActivityNotFoundException e) {
			Toast.makeText(activity, "Failed to share this application", Toast.LENGTH_LONG).show();
		}
	}

	private void init() {
		log.v(this, "init()");
		int version = Configuration.getInstance(activity).getCurrentVersion();
		if (version != CrashHandler.getVersionCode(activity)) {
			Message msg = Message.obtain();
			msg.what = Constant.WHAT_DISPLAY_CHANGELOG;
			viewHandler.sendMessage(msg);
		}
	}

	private void displayChapter() {
		log.v(this, "displayChapter()");
		FragmentManager fm = activity.getSupportFragmentManager();
		if (fm.findFragmentByTag(ChapterFragment.class.getName())!=null) {
			fm.beginTransaction().remove(fm.findFragmentByTag(ChapterFragment.class.getName())).commit();
		}
		fm.beginTransaction()
			.add(R.id.contentFragment, ChapterFragment.newInstance(), ChapterFragment.class.getName())
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			.commit();
	}
	
	private void selectChapter(Chapter chapter) {
		log.v(this, "selectVerse("+chapter.getNo()+")");
		FragmentManager fm = activity.getSupportFragmentManager();
		fm.beginTransaction()
			.replace(R.id.contentFragment, VerseFragment.newInstance(chapter), VerseFragment.class.getName())
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			.addToBackStack(VerseFragment.class.getName())
			.commit();
	}
	
	private void selectVerse(SearchVerse verse) {
		log.v(this, "selectVerse("+verse.getIndex()+")");
		FragmentManager fm = activity.getSupportFragmentManager();
		fm.beginTransaction()
			.replace(R.id.contentFragment, VerseFragment.newInstance(verse), VerseFragment.class.getName())
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			.addToBackStack(VerseFragment.class.getName())
			.commit();
	}
}
