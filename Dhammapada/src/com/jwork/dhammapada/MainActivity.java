/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.jwork.dhammapada.controller.DhammapadaController;
import com.jwork.dhammapada.utility.Configuration;
import com.jwork.dhammapada.utility.CrashHandler;
import com.jwork.dhammapada.utility.LogUtility;

public class MainActivity extends FragmentActivity implements OnClickListener{
//	public static int THEME = R.style.AppBaseTheme;

	private Controller controller;
	private Handler viewHandler = new MyHandler(this);
	private LogUtility log = LogUtility.getInstance();
	private PopupWindow aboutWindow;
	private FrameLayout mainLayout;
			
	private static class MyHandler extends Handler {
		MainActivity activity;
		
		public MyHandler(MainActivity mainActivity) {
			this.activity = mainActivity;
		}

		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Constant.WHAT_DISPLAY_CHANGELOG:
				activity.displayChangeLog();
				break;
			}
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		log.v(this, "onCreate()");
		if (CrashHandler.getInstance().isCrashFlag()) {
			log.v(this, "Crash flag detected");
			showCrashDialog();
		} else {
//			setTheme(MainActivity.THEME);
//			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.activity_main);
			mainLayout = (FrameLayout)findViewById(R.id.mainLayout);
			mainLayout.getForeground().setAlpha(0);
			
			LayoutInflater inflater = (LayoutInflater)
					this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			aboutWindow = new PopupWindow(inflater.inflate(R.layout.activity_about, null, false)
					, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
			TextView text = ((TextView)aboutWindow.getContentView().findViewById(R.id.about_text));
			text.setText(Html.fromHtml(getString(R.string.about_html).replaceAll("\\{0\\}", CrashHandler.getVersionName(this)).replaceAll("\\{1\\}", ""+CrashHandler.getVersionCode(this))));
			text.setMovementMethod(LinkMovementMethod.getInstance());
			ImageButton button = ((ImageButton)aboutWindow.getContentView().findViewById(R.id.about_ok));
			button.setOnClickListener(this);

			controller = new DhammapadaController(this, viewHandler);

			Message message = Message.obtain();
			message.what = Constant.WHAT_DISPLAY_CHAPTER;
			controller.executeMessage(message);

			message = Message.obtain();
			message.what = Constant.WHAT_INIT;
			controller.executeMessage(message);

//			ActionBar actionBar = getSupportActionBar();
//			actionBar.hide();
//			actionBar.setTitle("Dhammapada");
//			actionBar.setSubtitle("Chapter");
//			actionBar.setLogo(R.drawable.ic_launcher);
//			actionBar.show();
		}
	}
	
	private void showCrashDialog() {
		final CrashHandler handler = CrashHandler.getInstance();
		final AlertDialog ad = new AlertDialog.Builder(this).create();
		ad.setTitle("Error Report");
		ad.setMessage("An error was detected. Reporting it will support faster bug fixing.");
		ad.setButton(AlertDialog.BUTTON_POSITIVE, "Report it", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				handler.clearCrashFlag();
				ad.dismiss();
				MainActivity.this.finish();
				handler.sendEmail(MainActivity.this);
//				MainActivity.this.startActivity(new Intent(MainActivity.this, MainActivity.class));
			}
		});
		ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Not now", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				handler.clearCrashFlag();
				ad.dismiss();
				MainActivity.this.finish();
				MainActivity.this.startActivity(new Intent(MainActivity.this, MainActivity.class));
			}
		});
		ad.show();
	}

	public void displayChangeLog() {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setTitle("Changelog");

		WebView wv = new WebView(this);
		wv.loadData(getString(R.string.changelog_dialog_text), "text/html", "utf-8");
		wv.setScrollContainer(true);
		dialog.setView(wv);

		dialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Configuration.getInstance(MainActivity.this).setCurrentVersion(CrashHandler.getVersionCode(MainActivity.this));
				dialog.dismiss();
			}
		});
		dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Rate It", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Configuration.getInstance(MainActivity.this).setCurrentVersion(CrashHandler.getVersionCode(MainActivity.this));
				dialog.dismiss();
				Uri uri = Uri.parse("market://details?id=" + MainActivity.this.getPackageName());
				Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
				try {
					MainActivity.this.startActivity(myAppLinkToMarket);
				} catch (ActivityNotFoundException e) {
					Toast.makeText(MainActivity.this, "Failed to find Market application", Toast.LENGTH_LONG).show();
				}
			}
		});
		dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Donate", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Configuration.getInstance(MainActivity.this).setCurrentVersion(CrashHandler.getVersionCode(MainActivity.this));
				dialog.dismiss();
				openDonate();
			}
		});
		dialog.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		log.v(this, "onCreateOptionsMenu()");
		new MenuInflater(this).inflate(R.menu.menu_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		log.v(this, "onOptionsItemSelected("+item.getTitle()+")");
		String appName = getString(R.string.app_name);
		String appPackage = getPackageName();
		switch (item.getItemId()) {
		case R.id.action_rate:
			Uri uri = Uri.parse("market://details?id=" + appPackage);
		    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		    try {
		        startActivity(myAppLinkToMarket);
		    } catch (ActivityNotFoundException e) {
		        Toast.makeText(this, "Failed to find Market application", Toast.LENGTH_LONG).show();
		    }
			break;
		case R.id.action_donate:
			openDonate();
			break;
		case R.id.action_share:
			Intent intentText = new Intent(Intent.ACTION_SEND);
			intentText.setType("text/plain");
			intentText.putExtra(Intent.EXTRA_SUBJECT, appName); 
			intentText.putExtra(Intent.EXTRA_TEXT, 
					"Check out " + appName + " on Google Play! https://play.google.com/store/apps/details?id=" + appPackage);
			try {
				startActivity(Intent.createChooser(intentText, getString(R.string.share_this_app_intent)));
			} catch (ActivityNotFoundException e) {
				Toast.makeText(this, "Failed to share this application", Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.action_about:
			aboutWindow.showAtLocation(this.findViewById(R.id.mainLayout), Gravity.CENTER, 0, 0);
			mainLayout.getForeground().setAlpha(200);
			break;
		case R.id.action_feedback:
			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			String subject = "["+Constant.APP_NAME+"] Feedback";
			sendIntent.putExtra(Intent.EXTRA_EMAIL,
					new String[] {"jwork.dhammapada.os@gmail.com"});
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			sendIntent.putExtra(Intent.EXTRA_TEXT, "\n-------------------------------\n"+CrashHandler.getPhoneInfo(this));
			sendIntent.setType("message/rfc822");
			startActivity(Intent.createChooser(sendIntent, "Email:"));
			break;
		}
		// TODO Auto-generated method stub
		return super.onContextItemSelected(item);
	}

	private void openDonate() {
		//https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=W6EKHHSM75UXN
		Intent i = new Intent(Intent.ACTION_VIEW, 
				Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=W6EKHHSM75UXN"));
		startActivity(i);
	}

	@Override
	public void onClick(View v) {
		aboutWindow.dismiss();
		mainLayout.getForeground().setAlpha(0);
	}

}
