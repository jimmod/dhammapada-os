/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;

import com.jwork.dhammapada.utility.LogUtility;

public abstract class Controller {
	
	protected LogUtility log = LogUtility.getInstance();
	protected Handler viewHandler;
	protected FragmentActivity activity;
	
	private Handler controllerHandler = new ControllerHandler(this);
	
	private static class ControllerHandler extends Handler {
		private Controller controller;
		
		public ControllerHandler(Controller controller) {
			this.controller = controller;
		}
		
		public void handleMessage(android.os.Message msg) {
			controller.handleMessage(msg);
		};
	}
	
	public Controller(FragmentActivity activity, Handler viewHandler) {
		this.activity = activity;
		this.viewHandler = viewHandler;
	}

	public abstract void handleMessage(Message msg);
	
	public Handler getHandler() {
		return controllerHandler;
	}

	public void executeMessage(Message message) {
		controllerHandler.sendMessage(message);
	}

}
