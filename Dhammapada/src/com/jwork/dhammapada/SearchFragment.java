/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.jwork.dhammapada.controller.DhammapadaController;
import com.jwork.dhammapada.model.Chapter;
import com.jwork.dhammapada.model.DhammapadaContentProvider;
import com.jwork.dhammapada.model.DhammapadaContentProvider.TableChapter;
import com.jwork.dhammapada.model.DhammapadaContentProvider.TableVerse;
import com.jwork.dhammapada.model.SearchVerse;
import com.jwork.dhammapada.utility.LogUtility;

public class SearchFragment extends ListFragment implements LoaderCallbacks<Cursor> {

	private LogUtility log = LogUtility.getInstance();

	private SimpleCursorAdapter mAdapter;
	private Controller controller;
	private String query;

	private static String[] LOADER_SEARCH_PROJECTION = new String[]{
		DhammapadaContentProvider.TableVerse._NAME+"."+DhammapadaContentProvider.TableVerse.ID
		, DhammapadaContentProvider.TableChapter.TOTAL
		, DhammapadaContentProvider.TableChapter.START
		, DhammapadaContentProvider.TableChapter.END
		, DhammapadaContentProvider.TableChapter.TITLE_PALI
		, DhammapadaContentProvider.TableChapter.TITLE_ENG
		, DhammapadaContentProvider.TableChapter.TITLE_ID
		, DhammapadaContentProvider.TableVerse.CHAPTER_NO
		, DhammapadaContentProvider.TableVerse.VERSE_NO
		, DhammapadaContentProvider.TableVerse.VERSE_INDEX
		, DhammapadaContentProvider.TableVerse.CONTENT_PALI
		, DhammapadaContentProvider.TableVerse.CONTENT_ID
		, DhammapadaContentProvider.TableVerse.CONTENT_ENG
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		log.v(this, "onActivityCreated()");
		super.onActivityCreated(savedInstanceState);

		query = getArguments().getString("query").trim().toLowerCase();

		mAdapter = new CustomAdapter(getActivity(),
				R.layout.search_item, null,
				new String[] { DhammapadaContentProvider.TableChapter.TITLE_PALI },
				new int[] { R.id.searchChapter }, 0, query);
		setListAdapter(mAdapter);

		controller = new DhammapadaController(getActivity(), null);

		Bundle bundle = new Bundle();
		bundle.putString("query", query);
		getLoaderManager().initLoader(0, bundle, this);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		log.d(this, "onCreateView()");

		View view = inflater.inflate(R.layout.chapter_list, container,
				false);
		if (view == null) {
			log.w(this, "Problem inflating view, returned null");
			return null;
		}

		View search = view.findViewById(R.id.button_search);
		if (search!=null) {
			search.setVisibility(View.INVISIBLE);
		} else {
			search = view.findViewById(R.id.sv_search);
			if (search!=null) {
				search.setVisibility(View.INVISIBLE);
			}
		}

		setHasOptionsMenu(true);
		//        getSherlockActivity().getSupportActionBar().setSubtitle("Search Result");
		//        getSherlockActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		return view;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		log.v(this, "onCreateLoader(id:"+id+")");
		Uri baseUri;
		baseUri = DhammapadaContentProvider.getSearchUri(args.getString("query"));
		return new CursorLoader(getActivity(), baseUri,
				LOADER_SEARCH_PROJECTION, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		log.v(this, "onLoadFinished(data:"+data.getCount()+")");
		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		log.v(this, "onLoaderReset()");
		mAdapter.swapCursor(null);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		log.v(this, "onListItemClick(pos:"+position+"|id:"+id);
		Message message = Message.obtain();
		message.what = Constant.WHAT_SELECT_VERSE;
		message.obj = v.getTag();
		controller.executeMessage(message);
	}

	public static Fragment newInstance(String query) {
		SearchFragment myFragment = new SearchFragment();
		Bundle args = new Bundle();
		args.putSerializable("query", query);
		myFragment.setArguments(args);
		return myFragment;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		log.v(this, "onOptionsItemSelected("+item.getItemId()+")");
		Message message;
		switch (item.getItemId()) {
		case android.R.id.home:
			message = Message.obtain();
			message.what = Constant.WHAT_GOTO_BACK;
			message.obj = this;
			controller.executeMessage(message);
			break;
		case 99:
			break;
		default:
			//			Toast.makeText(getActivity(), "Unknown", Toast.LENGTH_SHORT).show();
			break;
		}
		return true;
	}

	private static class CustomAdapter extends SimpleCursorAdapter {

		private LayoutInflater layoutInflater;
		private int layout;
		private Typeface tf;
		private String query;
		private LogUtility log = LogUtility.getInstance();

		public CustomAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to, int flags, String query) {
			super(context, layout, c, from, to, flags);
			layoutInflater = LayoutInflater.from(context);
			this.layout = layout;
			tf = Typeface.createFromAsset(context.getAssets(), "fonts/arlrdbd.ttf");
			this.query = query;
		} 

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view = layoutInflater.inflate(layout, parent, false);

			Chapter chapter = new Chapter();
			chapter.setNo(cursor.getInt(cursor.getColumnIndex(TableVerse.VERSE_NO)));
			chapter.setStart(cursor.getInt(cursor.getColumnIndex(TableChapter.START)));
			chapter.setEnd(cursor.getInt(cursor.getColumnIndex(TableChapter.END)));
			chapter.setTotal(cursor.getInt(cursor.getColumnIndex(TableChapter.TOTAL)));
			chapter.setTitlePali(cursor.getString(cursor.getColumnIndex(TableChapter.TITLE_PALI)));
			chapter.setTitleEng(cursor.getString(cursor.getColumnIndex(TableChapter.TITLE_ENG)));
			chapter.setTitleId(cursor.getString(cursor.getColumnIndex(TableChapter.TITLE_ID)));

			SearchVerse verse = new SearchVerse(chapter, cursor);
			if (verse.getContentPali().indexOf(query)>=0) {
				verse.setSearchContent(getQueryPart(verse.getContentPali(), query));
			} else if (verse.getContentEng().indexOf(query)>=0) {
				verse.setSearchContent(getQueryPart(verse.getContentEng(), query));
			} else if (verse.getContentId().indexOf(query)>=0) {
				verse.setSearchContent(getQueryPart(verse.getContentId(), query));
			} 
			verse.setQuery(query);
			view.setTag(verse);

			return view;
		}

		private String getQueryPart(String content, String query) {
			log.v(this, "getQueryPart(content:"+content+"|query:"+query+")");
			content = content.toLowerCase();
			int pos = content.indexOf(query);
			int start = pos-30;
			if (start<0) {
				start = 0;
			}
			String result = content.substring(start, pos);
			result += "<b>"+query+"</b>";
			int end = pos + (query.length());
			if (result.length() <= 70) {
				end += 70-result.length();
			}
			if (end > content.length()) {
				end = content.length();
			}
			log.d(this, "start:"+start+"|end:"+end);
			result += content.substring(pos+query.length(), end);
			return result.replaceAll("\n", " ");
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			Chapter chapter = new Chapter();
			chapter.setNo(cursor.getInt(cursor.getColumnIndex(TableVerse.VERSE_NO)));
			chapter.setStart(cursor.getInt(cursor.getColumnIndex(TableChapter.START)));
			chapter.setEnd(cursor.getInt(cursor.getColumnIndex(TableChapter.END)));
			chapter.setTotal(cursor.getInt(cursor.getColumnIndex(TableChapter.TOTAL)));
			chapter.setTitlePali(cursor.getString(cursor.getColumnIndex(TableChapter.TITLE_PALI)));
			chapter.setTitleEng(cursor.getString(cursor.getColumnIndex(TableChapter.TITLE_ENG)));
			chapter.setTitleId(cursor.getString(cursor.getColumnIndex(TableChapter.TITLE_ID)));

			SearchVerse verse = new SearchVerse(chapter, cursor);
			if (verse.getContentPali().toLowerCase().indexOf(query.toLowerCase())>=0) {
				verse.setSearchContent(getQueryPart(verse.getContentPali(), query));
			} else if (verse.getContentEng().toLowerCase().indexOf(query.toLowerCase())>=0) {
				verse.setSearchContent(getQueryPart(verse.getContentEng(), query));
			} else if (verse.getContentId().toLowerCase().indexOf(query.toLowerCase())>=0) {
				verse.setSearchContent(getQueryPart(verse.getContentId(), query));
			} 
			verse.setQuery(query);
			view.setTag(verse);
			log.v(this, "verse.getIndex():"+verse.getIndex()+"|"+verse.getContentPali());
			log.v(this, "verse.getIndex():"+verse.getIndex()+"|"+verse.getContentEng());
			log.v(this, "verse.getIndex():"+verse.getIndex()+"|"+verse.getContentId());

			TextView txtSearchChapter = (TextView)view.findViewById(R.id.searchChapter);
			TextView txtSearchVerseIndex = (TextView)view.findViewById(R.id.searchVerseIndex);
			TextView txtSearchVerseContent = (TextView)view.findViewById(R.id.searchVerseContent);

			txtSearchChapter.setTypeface(tf);
			txtSearchVerseIndex.setTypeface(tf);
			//		    txtSearchVerseContent.setTypeface(tf);

			txtSearchChapter.setText(verse.getChapter().getTitlePali());
			txtSearchVerseIndex.setText(""+verse.getIndex());
			log.v(this, "verse.getSearchContent():"+verse.getSearchContent());
			txtSearchVerseContent.setText(Html.fromHtml(verse.getSearchContent()));

		}

	}

	//	@Override
	//	public boolean onQueryTextSubmit(String query) {
	//		log.v(this, "onQueryTextSubmit("+query+")");
	//		Message message = Message.obtain();
	//		message.what = Constant.WHAT_SEARCH_VERSE;
	//		message.obj = query;
	//		controller.executeMessage(message);
	//		return false;
	//	}
	//
	//	@Override
	//	public boolean onQueryTextChange(String newText) {
	//		// TODO Auto-generated method stub
	//		return false;
	//	}

}
