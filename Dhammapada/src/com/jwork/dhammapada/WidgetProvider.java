/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import java.util.Arrays;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.RemoteViews;

import com.jwork.dhammapada.model.DhammapadaContentProvider;
import com.jwork.dhammapada.model.Verse;
import com.jwork.dhammapada.utility.Configuration;
import com.jwork.dhammapada.utility.LogUtility;

public class WidgetProvider extends AppWidgetProvider {

	private LogUtility log = LogUtility.getInstance();
	private static String[] LOADER_CHAPTER_PROJECTION = new String[]{
		DhammapadaContentProvider.TableVerse.ID
		, DhammapadaContentProvider.TableVerse.CHAPTER_NO
		, DhammapadaContentProvider.TableVerse.VERSE_NO
		, DhammapadaContentProvider.TableVerse.VERSE_INDEX
		, DhammapadaContentProvider.TableVerse.CONTENT_PALI
		, DhammapadaContentProvider.TableVerse.CONTENT_ID
		, DhammapadaContentProvider.TableVerse.CONTENT_ENG
		};
	
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		log.v(this, "onEnabled");
	}

	@Override
	public void onDisabled(Context context) {
		super.onDisabled(context);
		log.v(this, "onDisabled");
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		super.onDeleted(context, appWidgetIds);
		log.v(this, "onDeleted:"+Arrays.toString(appWidgetIds));
		for (int id:appWidgetIds) {
			Configuration.getInstance(context).deleteWidgetConfiguration(id);
		}
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		log.v(this, "onUpdate:"+Arrays.toString(appWidgetIds));
		Configuration config = Configuration.getInstance(context);
		for (int id : appWidgetIds) {
			int language = config.getWidgetLanguage(id);
			long timestamp = config.getWidgetTimestamp(id);
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget_verse);
			String verseText;
			if (timestamp>System.currentTimeMillis()-Constant.WIDGET_UPDATE_TIME) {
				verseText = config.getWidgetVerse(id);
				remoteViews.setTextViewText(R.id.widgetVerse, verseText);
			} else {
				//			remoteViews.setOnClickPendingIntent(R.id.widgetLayout, pendingIntent);
				Cursor mCursor = context.getContentResolver().query(
						DhammapadaContentProvider.getVerseIndexUri(-1),   
						LOADER_CHAPTER_PROJECTION,                        
						null,                    
						null,                     
						null); 
				if (mCursor!=null && mCursor.moveToFirst()) {
					Verse verse = new Verse(null, mCursor);
					switch (language) {
					case 1:
						verseText = verse.getContentEng();
						break;
					case 2:
						verseText = verse.getContentId();
						break;
					default:
						verseText = verse.getContentPali();
						break;
					}
					remoteViews.setTextViewText(R.id.widgetVerse, verseText);
					config.updateWidgetTimestamp(id, verseText);
				}
			}
			appWidgetManager.updateAppWidget(id, remoteViews);
		}
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		int id = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
		log.v(this, "onReceive:"+id);
		if (id!=-1) {
			onUpdate(context, AppWidgetManager.getInstance(context), new int[]{id});
		} else {
			ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);
			int[] ids=AppWidgetManager.getInstance(context).getAppWidgetIds(thisWidget);
			onUpdate(context, AppWidgetManager.getInstance(context), ids);
		}
	}

	public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
			int appWidgetId){
		LogUtility log = LogUtility.getInstance();
		log.v(WidgetProvider.class, "updateAppWidget(id:"+appWidgetId+")");

		new WidgetProvider().onUpdate(context, appWidgetManager, new int[]{appWidgetId});
	}
	
}
