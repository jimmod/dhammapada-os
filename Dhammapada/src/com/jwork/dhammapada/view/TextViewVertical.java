/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewVertical extends TextView{
	
	public TextViewVertical(Context context) {
		super(context);
	}
	
	public TextViewVertical(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TextViewVertical(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	public void setText(CharSequence text, BufferType type) {
		StringBuffer result = new StringBuffer();
		for (int i=0;i<text.length();i++) {
			if (i==text.length()-1) {
				result.append(text.charAt(i));
			} else {
				result.append(text.charAt(i)+"\n");
			}
		}
		super.setText(result.toString(), type);
	}

}
