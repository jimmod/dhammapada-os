/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jwork.dhammapada.controller.DhammapadaController;
import com.jwork.dhammapada.model.Chapter;
import com.jwork.dhammapada.model.SearchVerse;
import com.jwork.dhammapada.utility.LogUtility;

public class VerseFragment extends Fragment implements OnClickListener, OnPageChangeListener {

	private LogUtility log = LogUtility.getInstance();

	private View view;
	private TextView tvTitlePali;
	private TextView tvTitleEng;
	private TextView tvTitleId;
	private TextView tvVerseIndex;
	private ViewPager mPager;
	private VerseSlidePagerAdapter mPagerAdapter;

	private int verseIndex;
	private DhammapadaController controller;

	private ImageButton btnFirst, btnPrev, btnNext, btnLast, btnHome, btnShare;
	private Chapter chapter;

	private ContentFragment currentContentFragment;
	private String searchQuery = null;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		log.v(this, "onActivityCreated()");
		super.onActivityCreated(savedInstanceState);

		chapter = (Chapter)getArguments().getSerializable("chapter");
		verseIndex = chapter.getStart();
		if (getArguments().containsKey("verseIndex")) {
			verseIndex = getArguments().getInt("verseIndex"); 
		}
		if (getArguments().containsKey("searchQuery")) {
			searchQuery = getArguments().getString("searchQuery"); 
		}

		tvTitlePali.setText(chapter.getTitlePali());
		tvTitleEng.setText(chapter.getTitleEng());
		tvTitleId.setText(chapter.getTitleId());
		tvVerseIndex.setText(verseIndex + " - " + (verseIndex-chapter.getStart()+1) + " " + getString(R.string.of) + " " + chapter.getTotal());

        controller = new DhammapadaController(this.getActivity(), null);
        
//        getSherlockActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        log.d(this, "onCreateView()");
        
        view = inflater.inflate(R.layout.verse, container,
                false);
        if (view == null) {
            log.e(this, "Problem inflating view, returned null");
            return null;
        }
        
        setHasOptionsMenu(true);
//        getSherlockActivity().getSupportActionBar().setSubtitle(getString(R.string.verse));
        
        tvTitlePali = (TextView)view.findViewById(R.id.verseTitlePali);
        tvTitleEng = (TextView)view.findViewById(R.id.verseTitleEng);
        tvTitleId = (TextView)view.findViewById(R.id.verseTitleId);
        tvVerseIndex = (TextView)view.findViewById(R.id.verseIndex);

        mPager = (ViewPager)view.findViewById(R.id.pager);
        mPagerAdapter = new VerseSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(this);
        
        btnFirst = (ImageButton)view.findViewById(R.id.button_first);
        btnPrev = (ImageButton)view.findViewById(R.id.button_previous);
        btnNext = (ImageButton)view.findViewById(R.id.button_next);
        btnLast = (ImageButton)view.findViewById(R.id.button_last);
        btnHome = (ImageButton)view.findViewById(R.id.button_home);
        btnShare = (ImageButton)view.findViewById(R.id.button_share);
        btnFirst.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnLast.setOnClickListener(this);
        btnHome.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        
        return view;
	}
	
	@Override
	public void onResume() {
		log.v(this, "onResume()");
		super.onResume();
	}
	
//	@Override
//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//		menu.clear();
//		inflater.inflate(R.menu.menu_verse, menu);
//	}
	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		Message message = null;
//		switch (item.getItemId()) {
////		case R.id.action_favorites:
////			Toast.makeText(getActivity(), "Favorite", Toast.LENGTH_SHORT).show();
////			break;
//		case R.id.action_share:
//			if (currentContentFragment==null) {
//				currentContentFragment = mPagerAdapter.getRegisteredFragment(0);
//			}
//			message = Message.obtain();
//			message.what = Constant.WHAT_SHARE_VERSE;
//			message.obj = currentContentFragment.getVerseCurrent();
//			controller.executeMessage(message);
//			break;
//		case android.R.id.home:
//			message = Message.obtain();
//			message.what = Constant.WHAT_GOTO_BACK;
//			message.obj = this;
//			controller.executeMessage(message);
//			break;
//		default:
//			Toast.makeText(getActivity(), "Unknown " + item.getItemId(), Toast.LENGTH_SHORT).show();
//			break;
//		}
//		return true;
//	}

	private void movePrevious() {
		log.v(this, "movePrevious()");
		verseIndex--;
		if (verseIndex<chapter.getStart()) {
			verseIndex = chapter.getStart();
		}
		updateContent();
	}
	
	private void moveNext() {
		log.v(this, "moveNext()");
		verseIndex++;
		if (verseIndex>chapter.getEnd()) {
			verseIndex = chapter.getEnd();
		}
		updateContent();
	}

	private void moveFirst() {
		log.v(this, "moveFirst()");
		verseIndex = chapter.getStart();
		updateContent();
	}
	
	private void moveLast() {
		log.v(this, "moveLast()");
		verseIndex = chapter.getEnd();
		updateContent();
	}
	
	private void updateContent() {
		mPager.setCurrentItem(verseIndex-chapter.getStart());
	}

	@Override
	public void onClick(View view) {
		log.v(this, "onClick("+view.getId()+")");
		if (view == btnPrev) {
			movePrevious();
		} else if (view == btnNext) {
			moveNext();
		} else if (view == btnFirst) {
			moveFirst();
		} else if (view == btnLast) {
			moveLast();
		} else if (view == btnShare) {
			shareVerse();
		} else if (view == btnHome) {
			back();
		}
	}

	private void back() {
		Message message = Message.obtain();
		message.what = Constant.WHAT_GOTO_BACK;
		message.obj = this;
		controller.executeMessage(message);
	}

	private void shareVerse() {
		if (currentContentFragment==null) {
			currentContentFragment = mPagerAdapter.getRegisteredFragment(0);
		}
		Message message = Message.obtain();
		message.what = Constant.WHAT_SHARE_VERSE;
		message.obj = currentContentFragment.getVerseCurrent();
		controller.executeMessage(message);
	}
	
	private class VerseSlidePagerAdapter extends FragmentStatePagerAdapter {
		private SparseArray<ContentFragment> registeredFragments = new SparseArray<ContentFragment>();
		private boolean isNew=true;
		
		public VerseSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	log.v(this, "getItem("+position+")");
            return ContentFragment.newInstance(chapter, chapter.getStart()+position, searchQuery);
        }

        @Override
        public int getCount() {
//        	log.v(this, "getCount()");
            return chapter.getTotal();
        }
        
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, (ContentFragment)fragment);
            if (isNew && position==1) {
            	isNew = false;
            	new Thread() {
            		public void run() {
            			try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
						}
            			getActivity().runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								updateContent();
							}
						});
            		};
            	}.start();
            }
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public ContentFragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
        
    }

	@Override
	public void onPageScrollStateChanged(int state) {
		log.v(this, "onPageScrollStateChanged("+state+")");
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//		log.v(this, "onPageScrolled(pos:"+position+"|offset:"+positionOffset+"|pixels:"+positionOffsetPixels+")");
	}

	@Override
	public void onPageSelected(int position) {
		log.v(this, "onPageSelected("+position+")");
		verseIndex = chapter.getStart()+position;
		tvVerseIndex.setText(verseIndex + " - " + (verseIndex-chapter.getStart()+1) + " " + getString(R.string.of) + " " + chapter.getTotal());
		currentContentFragment = (ContentFragment)mPagerAdapter.getRegisteredFragment(position);
	}

	public static Fragment newInstance(SearchVerse verse) {
		Bundle args = new Bundle();
	    args.putSerializable("chapter", verse.getChapter());
	    args.putInt("verseIndex", verse.getIndex());
	    if (verse.getQuery()!=null) {
	    	args.putString("searchQuery", verse.getQuery());
	    }
	    VerseFragment myFragment = new VerseFragment();
	    myFragment.setArguments(args);
		return myFragment;
	}
	
	public static Fragment newInstance(Chapter chapter) {
		Bundle args = new Bundle();
	    args.putSerializable("chapter", chapter);
	    VerseFragment myFragment = new VerseFragment();
	    myFragment.setArguments(args);
		return myFragment;
	}
	
}
