/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */

package com.jwork.dhammapada.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

/**
 * @author Jimmy Halim
 */
public class CrashHandler implements UncaughtExceptionHandler {
	
	private static final String FILE_STACK_TRACE = "stack.trace";

	private static final String TAG = "dhammapada";

	private static final String PREFS_CRASH = "crashed";

	private static final String APP_NAME = "Dhammapada OS";
	
	private static CrashHandler instance;
	private Context context;
	private LogUtility log;
	private UncaughtExceptionHandler defaultUEH;
	private String directory;
	SharedPreferences prefs;
	
	private CrashHandler(Context context, UncaughtExceptionHandler defaultUEH, String directory) {
		this.context = context;
		this.defaultUEH = defaultUEH;
		this.directory = directory;
		this.log = LogUtility.getInstance();
		this.prefs = context.getSharedPreferences(TAG, 0);
	}
	
	public synchronized static CrashHandler getInstance(Context context, UncaughtExceptionHandler defaultUEH, String directory) {
		if (instance==null) {
			instance = new CrashHandler(context, defaultUEH, directory);
		}
		return instance;
	}
	
	public synchronized static CrashHandler getInstance() {
		return instance;
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		try {
			log.e(this, ex);
			//TODO pre-crash
			setCrashFlag();
			
			String errorContent = createErrorReport(ex);
			File file = null;
			File directory = new File(this.directory);
			FileOutputStream trace = null;
			try {
				if (!directory.exists()) {
					directory.mkdir();
				}
				file = new File( directory.getAbsolutePath()+ "/" + FILE_STACK_TRACE);
				log.d(this, "Log file:" + file.getAbsolutePath());
				trace = new FileOutputStream(file);
				trace.write(errorContent.toString().getBytes());
			} catch(IOException ioe) {
				log.w(this, ioe);
			} finally {
				if (trace!=null) {
					try {
						trace.close();
					} catch (IOException e) {}
				}
			}
		} catch (Throwable ignore) {
			log.e(this, ignore);
		} finally {
			//TODO post-crash
			defaultUEH.uncaughtException(thread, ex);
		}
	}

	private void setCrashFlag() {
		prefs.edit().putBoolean(PREFS_CRASH, true).commit();
	}
	
	public boolean isCrashFlag() {
		return prefs.getBoolean(PREFS_CRASH, false);
	}
	
	public boolean clearCrashFlag() {
		return prefs.edit().remove(PREFS_CRASH).commit();
	}

	public void sendEmail(Activity activity)
	{
		BufferedReader br = null;
		try{
			File file = new File( directory + "/" + FILE_STACK_TRACE);
			br = new BufferedReader(new FileReader(file));
			String line;
			StringBuffer report = new StringBuffer();
			while ((line=br.readLine())!=null) {
				report.append(line);
				report.append("\n");
			}
	
			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			String subject = "["+APP_NAME+"] Error report";
			sendIntent.putExtra(Intent.EXTRA_EMAIL,
					new String[] {"jim.halim@gmail.com"});
			sendIntent.putExtra(Intent.EXTRA_TEXT, report.toString());
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			
			sendIntent.setType("message/rfc822");
	
			activity.startActivity(Intent.createChooser(sendIntent, "Email:"));
	
		} catch(Exception e) {
			Log.v("sendmail", e.toString());
		} finally {
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {}
			}
		}
	}
	
	public String createErrorReport(Throwable ex) {
		StringBuffer report = new StringBuffer();
		StackTraceElement[] arr = ex.getStackTrace();
		report.append("Please write description. ");
		report.append("Example: The app crashed when I just start the application or It crash when I press a button");
		report.append("\n\n-------------------------------\n");
		report.append(ex.toString()+"\n\n");
		report.append("--------- Stack trace ---------\n");
		for (int i=0; i<arr.length; i++)
		{
			report.append("    "+arr[i].toString()+"\n");
		}
		report.append("-------------------------------\n\n");

		report.append("--------- Cause ---------\n");
		Throwable cause = ex.getCause();
		if(cause != null) {
			report.append(cause.toString() + "\n");
			arr = cause.getStackTrace();
			for (int i=0; i<arr.length; i++)
			{
				report.append("    "+arr[i].toString()+"\n");
			}
		}
		report.append("-------------------------------\n\n");
		report.append(getPhoneInfo(context));
		report.append("-------------------------------\n");
//		report.append(additionalInfo+"\n");
//		report.append("-------------------------------\n");
		return report.toString();
	}

	public static String getPhoneInfo(Context activity) {
		StringBuffer info = new StringBuffer();
		info.append("App version: " + getVersionName(activity) + "(" + getVersionCode(activity) + ")" + '\n');
		info.append("Phone Model: " + android.os.Build.MODEL + '\n');
		info.append("Android Version: " + android.os.Build.VERSION.RELEASE + '\n');
		info.append("Board: " + android.os.Build.BOARD + '\n');
		info.append("Brand: " + android.os.Build.BRAND + '\n');
		info.append("Device: " + android.os.Build.DEVICE + '\n');
		info.append("Host: " + android.os.Build.HOST + '\n');
		info.append("ID: " + android.os.Build.ID + '\n');
		info.append("Product: " + android.os.Build.PRODUCT + '\n');
		info.append("Type: " + android.os.Build.TYPE + '\n');
		return info.toString();
	}
	
	public static int getVersionCode(Context activity) {
		int v = 0;
		try {
			v = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {}
		return v;
	}
	
	public static String getVersionName(Context activity) {
		String v = "0.0";
		try {
			v = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {}
		return v;
	}

}
