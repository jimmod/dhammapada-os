/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada.utility;

import android.util.Log;

/**
 * @author Jimmy Halim
 */
public class LogUtility {
	
	private static LogUtility instance;
	
	private LogUtility() {
	}
	
	public synchronized static LogUtility getInstance() {
		if (instance==null) {
			instance = new LogUtility();
		}
		return instance;
	}
	
	public void v(Object obj, String message) {
			Log.v(convertTag(obj), message);
	}

	private String convertTag(Object obj) {
		if (obj instanceof String) {
			return "c:"+obj.toString();
		}
		return "c:"+obj.getClass().getSimpleName();
	}

	public void v(Class<?> cls, String message) {
		Log.v("c:"+cls.getSimpleName(), message);
	}

	public void d(Object obj, String message) {
		Log.d(convertTag(obj), message);
	}

	public void i(Object obj, String message) {
		Log.i(convertTag(obj), message);
	}
	
	public void w(Object obj, String message) {
		Log.w(convertTag(obj), message);
	}
	
	public void e(Object obj, String message) {
		Log.e(convertTag(obj), message);
	}
	
	public void w(Object obj, Throwable e) {
		Log.e(convertTag(obj), e.getClass().getName(), e);
	}
	
	public void e(Object obj, Throwable e) {
		Log.e(convertTag(obj), e.getClass().getName(), e);
	}
}
