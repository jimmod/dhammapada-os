/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

public class Configuration {
	
	private static final String TAG = "dhammapada";
	private static final String KEY_LANGUAGE = "language";
	private static final String APP_NAME = "Dhammapada";
	private static final String PREFS_WIDGET_LANGUAGE = "widgetLanguage";
	private static final String PREFS_WIDGET_TIMESTAMP = "widgetTimestamp";
	private static final String PREFS_WIDGET_VERSE = "widgetVerse";
	private static final String PREFS_CURRENT_VERSION = "currentVersion";
	
	private static Configuration instance;
	private SharedPreferences prefs;
	private LogUtility log = LogUtility.getInstance();
	
	public Configuration(Context context) {
		prefs = context.getSharedPreferences(TAG, 0);
	}

	public synchronized static Configuration getInstance(Context context) {
		if (instance==null) {
			instance = new Configuration(context.getApplicationContext());
		}
		return instance;
	}
	
	public String getLanguage() {
		String data = prefs.getString(KEY_LANGUAGE, null);
		log.v(this, "getLanguage():"+data);
		return data;
	}
	
	public void setLanguage(String lang) {
		log.v(this, "setLanguage("+lang+")");
		prefs.edit().putString(KEY_LANGUAGE, lang).commit();
	}
	
	public String getAppDirectory() {
		String data = Environment.getExternalStorageDirectory().getPath() + "/" + APP_NAME;
		log.v(this, "getAppDirectory():"+data);
		return data;
	}

	public void setWidgetConfiguration(int id, int language) {
		log.v(this, "setWidgetConfiguration(id:"+id+"|language:"+language+")");
		prefs.edit().putInt(PREFS_WIDGET_LANGUAGE+id, language).commit();
	}

	public int getWidgetLanguage(int id) {
		int data = prefs.getInt(PREFS_WIDGET_LANGUAGE+id, 0);
		log.v(this, "getWidgetLanguage("+id+"):"+data);
		return data;
	}

	public void deleteWidgetConfiguration(int id) {
		log.v(this, "deleteWidgetConfiguration(id:"+id+")");
		prefs.edit().remove(PREFS_WIDGET_LANGUAGE+id).commit();
		prefs.edit().remove(PREFS_WIDGET_TIMESTAMP+id).commit();
		prefs.edit().remove(PREFS_WIDGET_VERSE+id).commit();
	}

	public void resetWidgetConfiguration(int id) {
		log.v(this, "resetWidgetConfiguration(id:"+id+")");
		prefs.edit().remove(PREFS_WIDGET_TIMESTAMP+id).commit();
		prefs.edit().remove(PREFS_WIDGET_VERSE+id).commit();
	}

	public long getWidgetTimestamp(int id) {
		long data = prefs.getLong(PREFS_WIDGET_TIMESTAMP+id, 0);
		log.v(this, "getWidgetTimestamp("+id+"):"+data);
		return data;
	}
	
	public String getWidgetVerse(int id) {
		String data = prefs.getString(PREFS_WIDGET_VERSE+id, null);
		log.v(this, "getWidgetVerse("+id+"):"+data);
		return data;
	}

	public void updateWidgetTimestamp(int id, String verseText) {
		log.v(this, "updateWidgetTimestamp(id:"+id+"|verse:"+verseText+")");
		prefs.edit().putLong(PREFS_WIDGET_TIMESTAMP+id, System.currentTimeMillis()).commit();
		prefs.edit().putString(PREFS_WIDGET_VERSE+id, verseText).commit();
	}
	
	public int getCurrentVersion() {
		int data = prefs.getInt(PREFS_CURRENT_VERSION, 0);
		log.v(this, "getCurrentVersion():"+data);
		return data;
	}

	public void setCurrentVersion(int version) {
		log.v(this, "setCurrentVersion(version:"+version+")");
		prefs.edit().putInt(PREFS_CURRENT_VERSION, version).commit();
	}

}
