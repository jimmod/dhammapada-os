/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jwork.dhammapada.model.Chapter;
import com.jwork.dhammapada.model.DhammapadaContentProvider;
import com.jwork.dhammapada.model.Verse;
import com.jwork.dhammapada.utility.LogUtility;

public class ContentFragment extends Fragment implements LoaderCallbacks<Cursor> {

	private static final String KEY_VERSE_INDEX = "verseIndex";
	private static final String KEY_CHAPTER_INDEX = "chapter";
	private static final String KEY_SEARCH_QUERY = "searchQuery";
	private LogUtility log = LogUtility.getInstance();

	private TextView tvContentPali;
	private TextView tvContentEng;
	private TextView tvContentId;

	private Typeface tfPali;
	private int verseIndex;
	private Verse verseCurrent;
	private Chapter chapter;
	private String searchQuery;
	
	private static int LOADER_VERSE = 2;
	private static String[] LOADER_CHAPTER_PROJECTION = new String[]{
		DhammapadaContentProvider.TableVerse.ID
		, DhammapadaContentProvider.TableVerse.CHAPTER_NO
		, DhammapadaContentProvider.TableVerse.VERSE_NO
		, DhammapadaContentProvider.TableVerse.VERSE_INDEX
		, DhammapadaContentProvider.TableVerse.CONTENT_PALI
		, DhammapadaContentProvider.TableVerse.CONTENT_ID
		, DhammapadaContentProvider.TableVerse.CONTENT_ENG
		};

	public static Fragment newInstance(Chapter chapter, int verseIndex, String searchQuery) {
		LogUtility.getInstance().v(ContentFragment.class, "newInstance("+chapter.getNo()+"|"+verseIndex+"|"+searchQuery+")");
		Bundle args = new Bundle();
	    args.putSerializable(KEY_VERSE_INDEX, verseIndex);
	    args.putSerializable(KEY_CHAPTER_INDEX, chapter);
	    if (searchQuery!=null) {
	    	args.putSerializable(KEY_SEARCH_QUERY, searchQuery);
	    }
	    ContentFragment myFragment = new ContentFragment();
	    myFragment.setArguments(args);
		return myFragment;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
        tfPali = Typeface.createFromAsset(getActivity().getAssets(), "fonts/arlrdbd.ttf");
        
        verseIndex = getArguments().getInt(KEY_VERSE_INDEX);
        chapter = (Chapter)getArguments().getSerializable(KEY_CHAPTER_INDEX);
        searchQuery = getArguments().getString(KEY_SEARCH_QUERY);
        
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_VERSE_INDEX, verseIndex);
        getLoaderManager().initLoader(LOADER_VERSE, bundle, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		log.d(this, "onCreateView()");
		View view = inflater.inflate(R.layout.verse_content, container,
				false);

		tvContentPali = (TextView)view.findViewById(R.id.verseContentPali);
		tvContentPali.setTypeface(tfPali, Typeface.NORMAL);
		tvContentEng = (TextView)view.findViewById(R.id.verseContentEng);
		tvContentId = (TextView)view.findViewById(R.id.verseContentId);

		return view;
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		int verseIndex = args.getInt(KEY_VERSE_INDEX);
		log.v(this, "onCreateLoader(id:"+id+"|"+verseIndex+")");
        Uri baseUri;
        baseUri = DhammapadaContentProvider.getVerseIndexUri(verseIndex);
        return new CursorLoader(getActivity(), baseUri, LOADER_CHAPTER_PROJECTION, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		log.v(this, "onLoadFinished()");
		boolean empty = !data.moveToFirst();
		if (!empty) {
			verseCurrent = new Verse(chapter, data);
			tvContentPali.setText(verseCurrent.getContentPali());
			tvContentEng.setText(Html.fromHtml(boldSearchQuery(verseCurrent.getContentEng())));
			tvContentId.setText(Html.fromHtml(boldSearchQuery(verseCurrent.getContentId())));
		}
	}

	private String boldSearchQuery(String content) {
		log.v(this, "boldSearchQuery("+content+")");
		if (searchQuery==null) return content;
		int pos = 0;
		while ((pos=content.indexOf(searchQuery, pos))>=0) {
			int total = content.length();
			content = content.substring(0, pos) + "<b>" + searchQuery + "</b>" + content.substring(pos+searchQuery.length(), total);
			pos += searchQuery.length() + 7;
		}
		log.d(this, "Result:"+content);
		return content;
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		log.v(this, "onLoaderReset()");
	}

	public Verse getVerseCurrent() {
		log.v(this, "getVerseCurrent():"+verseCurrent);
		return verseCurrent;
	}


}
