/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Application;

import com.jwork.dhammapada.utility.Configuration;
import com.jwork.dhammapada.utility.CrashHandler;
import com.jwork.dhammapada.utility.LogUtility;

public class MyApplication extends Application {

	private UncaughtExceptionHandler defaultUEH;
	private CrashHandler handler;
	private LogUtility log = LogUtility.getInstance();
	
	@Override
	public void onCreate() {
		log.v(this, "onCreate()");
		defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		String directory = Configuration.getInstance(getApplicationContext()).getAppDirectory();
		handler = CrashHandler.getInstance(getApplicationContext(), defaultUEH, directory);
		Thread.setDefaultUncaughtExceptionHandler(handler);
		super.onCreate();
	}
	
}
