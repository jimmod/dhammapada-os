/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Spinner;

import com.jwork.dhammapada.utility.Configuration;
import com.jwork.dhammapada.utility.LogUtility;

public class WidgetConfigure extends Activity {

	private LogUtility log = LogUtility.getInstance();
	private int mAppWidgetId = -1;
	private Spinner spinnerAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		log.v(this, "onCreate");
		
		setContentView(R.layout.widget_configure);
		spinnerAction = (Spinner) findViewById(R.id.widgetLanguage);
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
		    mAppWidgetId = extras.getInt(
		            AppWidgetManager.EXTRA_APPWIDGET_ID, 
		            AppWidgetManager.INVALID_APPWIDGET_ID);
		}
		log.d(this, "mAppWidgetId: " + mAppWidgetId);
	}
	
	public void onClickOK(View view) {
		log.v(this, "onClickOK");

		int language = spinnerAction.getSelectedItemPosition();
		
		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
		
		Configuration prefs = Configuration.getInstance(getApplicationContext());
		prefs.setWidgetConfiguration(mAppWidgetId, language);
		
		
		setResult(RESULT_OK, resultValue);

		prefs.resetWidgetConfiguration(mAppWidgetId);

		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
		RemoteViews views = new RemoteViews(getApplicationContext().getPackageName(),
				R.layout.widget_verse);
		appWidgetManager.updateAppWidget(mAppWidgetId, views);
		WidgetProvider.updateAppWidget(this, appWidgetManager, mAppWidgetId);

		finish();
	}
	
	public void onClickCancel(View view) {
		log.v(this, "onClickCancel");
		setResult(RESULT_CANCELED);
		finish();
	}
	
}
