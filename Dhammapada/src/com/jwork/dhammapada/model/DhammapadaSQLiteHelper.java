/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jwork.dhammapada.utility.LogUtility;

public class DhammapadaSQLiteHelper extends SQLiteOpenHelper {
//	private static String DATABASE_PATH = "/data/data/"+Constant.APP_PACKAGE+"/databases/";
	private static final String DATABASE_NAME = "database.db";
	private static final int DATABASE_VERSION = 10;

	private LogUtility log = LogUtility.getInstance();
	private Context context;

	public DhammapadaSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		log.v(this, "onCreate()");
		try {
			execSqlFromAssets("databases/data.sql", db);
		} catch (IOException e) {
			log.w(this, e);
			try {
				execSqlFromAssets("databases/data_sample.sql", db);
			} catch (IOException e1) {
				log.e(this, e);
			}
		}
	}

	public void execSqlFromAssets(String sqlFile, SQLiteDatabase db) throws IOException {
		log.v(this, "execSqlFromAssets("+sqlFile+")");
		InputStream input;
		BufferedReader reader;
		input = context.getAssets().open(sqlFile);
		reader = new BufferedReader(new InputStreamReader(input));

		String statement = null;
		String statementFull = "";

		while ((statement=reader.readLine())!=null) {
			statement = statement.trim();
			if (statementFull.length()==0 && statement.endsWith(");")) {
				if (statement.endsWith("--);")) {
					statement = statement.substring(0, statement.length()-4);
				}
				log.d(this, "execute:"+statement);
				db.execSQL(statement);
			} else {
				statementFull += "\n" + statement;
				if (statement.endsWith(");")) {
					log.d(this, "execute:"+statementFull);
					db.execSQL(statementFull);
					statementFull="";
				}
			}
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		log.v(this, "onUpgrade("+oldVersion+"->"+newVersion+")");
		try {
			execSqlFromAssets("databases/data.sql", db);
		} catch (IOException e) {
			log.w(this, e);
			try {
				execSqlFromAssets("databases/data_sample.sql", db);
			} catch (IOException e1) {
				log.e(this, e);
			}
		}
	}

//	private boolean checkDataBase(){
//		log.v(this, "checkDataBase()");
//		SQLiteDatabase checkDB = null;
//		try{
//			String myPath = DATABASE_PATH + DATABASE_NAME;
//			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
//		} catch(SQLiteException e){
//			//database does't exist yet.
//		} finally {
//			try {
//				checkDB.close();
//			} catch (Exception e) {}
//		}
//		boolean result = checkDB != null ? true : false;
//		log.d(this, "checkDataBase():"+result);
//		return result;
//	}
//	
//	private boolean doesDatabaseExist(Context context, String dbName) {
//	    File dbFile=context.getDatabasePath(dbName);
//	    boolean result = dbFile.exists();
//		log.d(this, "checkDataBase():"+result);
//		return result;
//	}

}
