/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada.model;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.jwork.dhammapada.Constant;
import com.jwork.dhammapada.utility.LogUtility;

public class DhammapadaContentProvider extends ContentProvider {

	private static LogUtility log = LogUtility.getInstance();
	private DhammapadaSQLiteHelper dbHelper;
	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	private static final int CHAPTER = 1;
	private static final int VERSE_CONTENT = 2;
	private static final int SEARCH = 3;
	private static final String CHAPTER_BASE_PATH = "chapter";
	private static final String VERSE_BASE_PATH = "verse";
	private static final String SEARCH_BASE_PATH = "search";
	
	public static class TableChapter  {
		public static final String _NAME = "chapter";
		public static final String ID = "_id";
		public static final String NO = "no";
		public static final String TOTAL = "total";
		public static final String START = "start";
		public static final String END = "end";
		public static final String TITLE_PALI = "title_pali";
		public static final String TITLE_ENG = "title_eng";
		public static final String TITLE_ID = "title_id";
	}
	
	public static class TableVerse {
		public static final String _NAME = "verse";
		public static final String ID = "_id";
		public static final String CHAPTER_NO = "chapter_no";
		public static final String VERSE_NO = "verse_no";
		public static final String VERSE_INDEX = "verse_index";
		public static final String CONTENT_PALI = "content_pali";
		public static final String CONTENT_ENG = "content_eng";
		public static final String CONTENT_ID = "content_id";
	}
	
	public DhammapadaContentProvider() {
		sURIMatcher.addURI(Constant.APP_PACKAGE, SEARCH_BASE_PATH+"/*", SEARCH);
		sURIMatcher.addURI(Constant.APP_PACKAGE, VERSE_BASE_PATH+"/*", VERSE_CONTENT);
	    sURIMatcher.addURI(Constant.APP_PACKAGE, CHAPTER_BASE_PATH, CHAPTER);
	}
	
	@Override
	public boolean onCreate() {
		log.v(this, "onCreate()");
		dbHelper = new DhammapadaSQLiteHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		log.v(this, "query("+uri+")");
	    SQLiteDatabase db = dbHelper.getWritableDatabase();
	    Cursor cursor;
	    SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    
	    int uriType = sURIMatcher.match(uri);
	    switch (uriType) {
	    case CHAPTER:
	    	queryBuilder.setTables(TableChapter._NAME);
	    	cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
	      break;
//	    case CHAPTER_VERSE:
//	    	queryBuilder.setTables(TableVerse._NAME);
//	    	selection = TableVerse.LANG + " = '" + uri.getPathSegments().get(1) +"'";
//	    	selection += " AND " + TableVerse.CHAPTER_NO + " = '" + uri.getPathSegments().get(2) +"'";
//	    	break;
	    case VERSE_CONTENT:
	    	queryBuilder.setTables(TableVerse._NAME);
	    	int verseIndex = -1;
	    	try {
	    		verseIndex = Integer.parseInt(uri.getPathSegments().get(1));
	    	} catch (NumberFormatException e) {}
	    	if (verseIndex>0) {
	    		selection = TableVerse.VERSE_INDEX + " = '" + verseIndex +"'";
	    	} else {
	    		selection = null;
	    		sortOrder = "RANDOM() LIMIT 1";
	    	}
	    	cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
	    	break;
	    case SEARCH:
	    	queryBuilder.setTables(TableChapter._NAME + " LEFT OUTER JOIN " + TableVerse._NAME + " ON ("+TableChapter.NO+"="+TableVerse.CHAPTER_NO+")");
	    	selection = TableVerse.CONTENT_PALI + " like '%" + uri.getPathSegments().get(1) + "%'"
	    			+ " OR " + TableVerse.CONTENT_ENG + " like '%" + uri.getPathSegments().get(1) + "%'"
	    			+ " OR " + TableVerse.CONTENT_ID + " like '%" + uri.getPathSegments().get(1) + "%'";
	    	cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
	      break;
	    default:
	      throw new IllegalArgumentException("Unknown URI: " + uri);
	    }

	    // Make sure that potential listeners are getting notified
	    cursor.setNotificationUri(getContext().getContentResolver(), uri);
	    log.d(this, "result:"+(cursor==null?"null":cursor.getCount()));
	    return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		log.v(this, "update("+uri+")");
		return 0;
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		log.v(this, "delete("+uri+")");
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		log.v(this, "getType("+uri+")");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		log.v(this, "insert("+uri+")");
		// TODO Auto-generated method stub
		return null;
	}

	public static Uri getVerseIndexUri(int verseNo) {
		String url = "content://"+Constant.APP_PACKAGE+"/"+VERSE_BASE_PATH+"/"+verseNo;
		log.v(DhammapadaContentProvider.class, "getVerseListContentUri("+url+")");
		return Uri.parse(url);
	}

	public static Uri getChapterUri() {
		log.v(DhammapadaContentProvider.class, "getChapterUri()");
		return Uri.parse("content://"+Constant.APP_PACKAGE+"/"+CHAPTER_BASE_PATH);
	}
	
	public static Uri getSearchUri(String query) {
		log.v(DhammapadaContentProvider.class, "getSearchUri("+query+")");
		return Uri.parse("content://"+Constant.APP_PACKAGE+"/"+SEARCH_BASE_PATH+"/"+query);
	}

}
