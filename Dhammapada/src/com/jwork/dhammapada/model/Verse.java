/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada.model;

import java.io.Serializable;

import android.database.Cursor;

import com.jwork.dhammapada.model.DhammapadaContentProvider.TableVerse;

public class Verse implements Serializable {

	private static final long serialVersionUID = 2479622988268311472L;
	
	private int chapterNo;
	private int index;
	private int no;
	private String contentPali;
	private String contentEng;
	private String contentId;
	
	private Chapter chapter;
	
	public Verse() {
	}
	
	public Verse(Chapter chapter, Cursor cursor) {
		this.chapter = chapter;
		chapterNo = cursor.getInt(cursor.getColumnIndex(TableVerse.CHAPTER_NO));
		index = cursor.getInt(cursor.getColumnIndex(TableVerse.VERSE_INDEX));
		no = cursor.getInt(cursor.getColumnIndex(TableVerse.VERSE_NO));
		contentPali = cursor.getString(cursor.getColumnIndex(TableVerse.CONTENT_PALI));
		contentEng = cursor.getString(cursor.getColumnIndex(TableVerse.CONTENT_ENG));
		contentId = cursor.getString(cursor.getColumnIndex(TableVerse.CONTENT_ID));
	}
	
	public int getChapterNo() {
		return chapterNo;
	}
	public void setChapterNo(int chapterNo) {
		this.chapterNo = chapterNo;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getContentPali() {
		return contentPali;
	}
	public void setContentPali(String contentPali) {
		this.contentPali = contentPali;
	}
	public String getContentEng() {
		return contentEng;
	}
	public void setContentEng(String contentEng) {
		this.contentEng = contentEng;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public Chapter getChapter() {
		return chapter;
	}
}
