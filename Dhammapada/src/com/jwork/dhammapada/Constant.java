/* ========================================================================
 * Copyright 2013 Jimmy Halim
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 license 
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://creativecommons.org/licenses/by-nc/3.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.dhammapada;

public class Constant {

	public static String APP_PACKAGE = "com.jwork.dhammapada";
	
	public static final int WHAT_DISPLAY_CHAPTER = 1;
	public static final int WHAT_SELECT_CHAPTER = 2;
	public static final int WHAT_SELECT_VERSE = 3;
	public static final int WHAT_INIT = 4;
	public static final int WHAT_DISPLAY_CHANGELOG = 5;
	public static final int WHAT_SHARE_VERSE = 6;
	public static final int WHAT_GOTO_BACK = 7;
	public static final int WHAT_SEARCH_VERSE = 8;

	public static final String APP_NAME = "Dhammapada OS";
	
	public static final long WIDGET_UPDATE_TIME = 60 * 60 * 1000;

}
