DROP TABLE IF EXISTS chapter;--);
DROP TABLE IF EXISTS verse;--);
CREATE TABLE chapter (_id INTEGER PRIMARY KEY, no NUMERIC, total NUMERIC, start NUMERIC, end NUMERIC, title_pali TEXT, title_id TEXT, title_eng TEXT);
CREATE TABLE verse (_id NUMERIC, chapter_no NUMERIC, verse_index NUMERIC, verse_no NUMERIC, content_pali TEXT, content_id TEXT, content_eng TEXT);
INSERT INTO chapter VALUES(1,1,20,1,20,"Yamaka Vagga","Syair berpasangan","The Pairs");
INSERT INTO chapter VALUES(2,2,12,21,32,"Appamada Vagga","Kewaspadaan","Mindfulness ");
INSERT INTO chapter VALUES(3,3,11,32,43,"Citta Vagga","Pikiran","The Mind");
INSERT INTO chapter VALUES(4,4,16,44,59,"Puppha Vagga","Bunga bunga","Flowers");
INSERT INTO chapter VALUES(5,5,16,60,75,"Bala Vagga","Orang bodoh","The Fool");
INSERT INTO chapter VALUES(6,6,14,76,89,"Pandita Vagga","Orang bijaksana","The Wise");
INSERT INTO chapter VALUES(7,7,10,90,99,"Arahanta Vagga","Arahat","The Arahant");
INSERT INTO chapter VALUES(8,8,16,100,115,"Sahassa Vagga","Ribuan","The Thousands");
INSERT INTO chapter VALUES(9,9,13,116,128,"Papa Vagga","Kejahatan","Evil");
INSERT INTO chapter VALUES(10,10,17,129,145,"Danda Vagga","Hukuman","Punishment");
INSERT INTO chapter VALUES(11,11,11,146,156,"Jara Vagga","Usia tua","Aging");
INSERT INTO chapter VALUES(12,12,10,157,166,"Atta Vagga","Diri sendiri","Self");
INSERT INTO chapter VALUES(13,13,12,167,178,"Loka Vagga","Dunia","The World");
INSERT INTO chapter VALUES(14,14,18,179,196,"Buddha Vagga","Buddha","The Buddha");
INSERT INTO chapter VALUES(15,15,12,197,208,"Sukha Vagga","Kebahagiaan","Happiness");
INSERT INTO chapter VALUES(16,16,12,209,220,"Piya Vagga","Kecintaan","Affection");
INSERT INTO chapter VALUES(17,17,14,221,234,"Kodha Vagga","Kemarahan","Anger");
INSERT INTO chapter VALUES(18,18,21,235,255,"Mala Vagga","Noda noda","Impurities");
INSERT INTO chapter VALUES(19,19,17,256,272,"Dhammattha Vagga","Orang adil","The Just or the Righteous");
INSERT INTO chapter VALUES(20,20,17,273,289,"Magga Vagga","Jalan","The Path");
INSERT INTO chapter VALUES(21,21,16,290,305,"Pakinnaka Vagga","Bunga rampai","Miscellaneous");
INSERT INTO chapter VALUES(22,22,14,306,319,"Niraya Vagga","Neraka","The Chapter on Niraya");
INSERT INTO chapter VALUES(23,23,14,320,333,"Naga Vagga","Gajah","The Elephant");
INSERT INTO chapter VALUES(24,24,26,334,359,"Tanha Vagga","Nafsu keinginan","Craving");
INSERT INTO chapter VALUES(25,25,23,360,382,"Bhikkhu Vagga","Bhikkhu","The Bhikkhu");
INSERT INTO chapter VALUES(26,26,41,383,423,"Brahmana Vagga","Brahmana","Brahmana or Arahat");
INSERT INTO verse VALUES(1,1,1,1,"Manopubbaṅgamā dhammā, manoseṭṭhā manomayā;
Manasā ce paduṭṭhena, bhāsati vā karoti vā;
Tato naṃ dukkhamanveti, cakkaṃva vahato padaṃ.","Pikiran adalah pelopor dari segala sesuatu,
pikiran adalah pemimpin,
pikiran adalah pembentuk.
Bila seseorang berbicara atau berbuat dengan pikiran jahat,
maka penderitaan akan mengikutinya,
bagaikan roda pedati mengikuti langkah kaki lembu yang menariknya.","All mental phenomena have mind as their forerunner. 
they have mind as their chief. 
they are mind-made. 
If one speaks or acts with an evil mind, 'dukkha'  follows him just as the wheel follows the hoofprint of the ox that draws the cart.");
INSERT INTO verse VALUES(2,1,2,2,"Manopubbaṅgamā dhammā, manoseṭṭhā manomayā;
Manasā ce pasannena, bhāsati vā karoti vā;
Tato naṃ sukhamanveti, chāyāva anapāyinī [anupāyinī (ka.)].","Pikiran adalah pelopor dari segala sesuatu,
pikiran adalah pemimpin,
pikiran adalah pembentuk.
Bila seseorang berbicara atau berbuat dengan pikiran murni,
maka kebahagiaan akan mengikutinya,
bagaikan bayang-bayang yang tak pernah meninggalkan bendanya.","All mental phenomena have mind as their forerunner.
they have mind as their chief.
they are mind-made. 
If one speaks or acts with a pure mind, happiness (sukha) follows him like a shadow that never leaves him.");
INSERT INTO verse VALUES(3,1,3,3,"Akkocchi maṃ avadhi maṃ, ajini [ajinī (?)] maṃ ahāsi me;
Ye ca taṃ upanayhanti, veraṃ tesaṃ na sammati.","""Ia menghina saya,
ia memukul saya,
ia mengalahkan saya,
ia merampas milik saya.""
Selama seseorang masih menyimpan pikiran seperti itu,
maka kebencian tak akan pernah berakhir.","""He abused me, 
he ill-treated me, 
he got the better of me, 
he stole my belongings.""... 
the enmity of those harbouring such thoughts cannot be appeased.");
INSERT INTO verse VALUES(4,1,4,4,"Akkocchi maṃ avadhi maṃ, ajini maṃ ahāsi me;
Ye ca taṃ nupanayhanti, veraṃ tesūpasammati.","""Ia menghina saya,
ia memukul saya,
ia mengalahkan saya,
ia merampas milik saya.""
Jika seseorang sudah tidak lagi menyimpan pikiran-pikiran seperti itu,
maka kebencian akan berakhir.","""He abused me, 
he ill-treated me, 
he got the better of me, 
he stole my belongings.""... 
the enmity of those not harbouring such thoughts can be appeased.");
INSERT INTO verse VALUES(5,1,5,5,"Na hi verena verāni, sammantīdha kudācanaṃ;
Averena ca sammanti, esa dhammo sanantano.","Kebencian tak akan pernah berakhir,
apabila dibalas dengan kebencian.
Tetapi, kebencian akan berakhir,
Bila dibalas dengan tidak membenci.
Inilah satu hukum abadi.","Hatred is, indeed, never appeased by hatred in this world. 
It is appeased only by loving-kindness. 
This is an ancient law.");
INSERT INTO verse VALUES(6,1,6,6,"Pare ca na vijānanti, mayamettha yamāmase;
Ye ca tattha vijānanti, tato sammanti medhagā.","Sebagian besar orang tidak mengetahui bahwa,
dalam pertengkaran mereka akan binasa.
tetapi mereka,
yang dapat menyadari kebenaran ini.
akan segera mengakhiri semua pertengkaran.","People, other than the wise, do not realize, 
""We in this world must all die,"" 
(and, not realizing it, continue their quarrels). 
The wise realize it and thereby their quarrels cease.");
INSERT INTO verse VALUES(7,1,7,7,"Subhānupassiṃ viharantaṃ, indriyesu asaṃvutaṃ;
Bhojanamhi cāmattaññuṃ, kusītaṃ hīnavīriyaṃ;
Taṃ ve pasahati māro, vāto rukkhaṃva dubbalaṃ.","Seseorang yang hidupnya hanya ditujukan pada hal-hal yang menyenangkan,
yang inderanya tidak terkendali,
yang makannya tidak mengenal batas,
malas serta tidak bersemangat,
maka Mara (Penggoda) akan menguasai dirinya.
bagaikan angin yang menumbangkan pohon yang lapuk.","He who keeps his mind on pleasant objects, 
who is uncontrolled in his senses, 
immoderate in his food, and is lazy and lacking in energy, 
will certainly be overwhelmed by Mara,
just as stormy winds uproot a weak tree.");
INSERT INTO verse VALUES(8,1,8,8,"Asubhānupassiṃ viharantaṃ, indriyesu susaṃvutaṃ;
Bhojanamhi ca mattaññuṃ, saddhaṃ āraddhavīriyaṃ;
Taṃ ve nappasahati māro, vāto selaṃva pabbataṃ.","Seseorang yang hidupnya tidak ditujukan pada hal-hal yang menyenangkan,
yang inderanya terkendali,
sederhana dalam makanan,
penuh keyakinan serta bersemangat,
maka Mara (Penggoda) tidak dapat menguasai dirinya.
bagaikan angin yang tidak dapat menumbangkan gunung karang.","He who keeps his mind on the impurities (of the body), 
who is well-controlled in his senses 
and is full of faith and energy, 
will certainly be not overwhelmed by Mara, 
just as stormy winds cannot shake a mountain of rock.");
INSERT INTO verse VALUES(9,1,9,9,"Anikkasāvo kāsāvaṃ, yo vatthaṃ paridahissati;
Apeto damasaccena, na so kāsāvamarahati.","Barang siapa yang belum bebas,
dari kekotoran-kekotoran batin.
yang tidak memiliki pengendalian diri,
serta tidak mengerti kebenaran.
sesungguhnya tidak patut,
ia mengenakan jubah kuning.","He who is not free from taints of moral defilements (kilesas) 
and yet dons the yellow robe, 
who lacks restraint in his senses 
and (speaks not the) truth is unworthy of the yellow robe.");
INSERT INTO verse VALUES(10,1,10,10,"Yo ca vantakasāvassa, sīlesu susamāhito;
Upeto damasaccena, sa ve kāsāvamarahati.","Tetapi, ia yang telah dapat,
membuang kekotoran-kekotoran batin,
teguh dalam kesusilaan.
memiliki pengendalian diri.
serta mengerti kebenaran.
maka sesungguhnya ia patut,
mengenakan jubah kuning.","He who has discarded all moral defilements (kilesas), 
who is established in moral precepts, 
is endowed with restraint 
and (speaks the) truth is, indeed, worthy of the yellow robe.");
INSERT INTO verse VALUES(11,1,11,11,"Asāre sāramatino, sāre cāsāradassino;
Te sāraṃ nādhigacchanti, micchāsaṅkappagocarā.","Mereka yang menganggap,
ketidak-benaran sebagai kebenaran.
dan kebenaran sebagai ketidak-benaran.
maka mereka yang mempunyai,
pikiran keliru seperti itu,
tak akan pernah dapat,
menyelami kebenaran.","They take untruth for truth. 
they take truth for untruth. 
such persons can never arrive at the truth, 
for they hold wrong views.");
INSERT INTO verse VALUES(12,1,12,12,"Sārañca sārato ñatvā, asārañca asārato;
Te sāraṃ adhigacchanti, sammāsaṅkappagocarā.","Mereka yang mengetahui,
kebenaran sebagai kebenaran.
dan ketidak-benaran sebagai ketidak-benaran,
maka mereka yang mempunyai,
pikiran benar seperti itu,
akan dapat menyelami kebenaran.","They take truth for truth.
they take untruth for untruth. 
such persons arrive at the truth, 
for they hold right views.");
INSERT INTO verse VALUES(13,1,13,13,"Yathā agāraṃ ducchannaṃ, vuṭṭhī samativijjhati;
Evaṃ abhāvitaṃ cittaṃ, rāgo samativijjhati.","Bagaikan hujan,
yang dapat menembus rumah beratap tiris.
demikian pula nafsu,
akan dapat menembus pikiran yang tidak dikembangkan dengan baik.","Just as rain penetrates a badly-roofed house, 
so also, passion (raga) penetrates a mind not cultivated in Tranquillity 
and Insight Development (Samatha and Vipassana).");
INSERT INTO verse VALUES(14,1,14,14,"Yathā agāraṃ suchannaṃ, vuṭṭhī na samativijjhati;
Evaṃ subhāvitaṃ cittaṃ, rāgo na samativijjhati.","Bagaikan hujan,
yang tidak dapat menembus rumah beratap baik.
demikian pula nafsu,
tidak dapat menembus pikiran yang telah dikembangkan dengan baik.","Just as rain cannot penetrate a well-roofed house, 
so also, passion (raga) cannot penetrate a mind well-cultivated in Tranquillity 
and Insight Development (Samatha and Vipassana).");
INSERT INTO verse VALUES(15,1,15,15,"Idha socati pecca socati, pāpakārī ubhayattha socati;
So socati so vihaññati, disvā kammakiliṭṭhamattano.","Di dunia ini ia bersedih hati.
di dunia sana ia bersedih hati.
pelaku kejahatan akan bersedih hati,
di kedua dunia itu.
ia bersedih hati dan meratap,
karena melihat perbuatannya sendiri,
yang tidak bersih.","Here he grieves, hereafter he grieves.
the evil-doer grieves in both existences.
He grieves and he suffers anguish when he sees the depravity of his own deeds.");
INSERT INTO verse VALUES(16,1,16,16,"Idha modati pecca modati, katapuñño ubhayattha modati;
So modati so pamodati, disvā kammavisuddhimattano.","Di dunia ini ia bergembira.
Di dunia sana ia bergembira.
Pelaku kebajikan,
bergembira di kedua dunia itu.
Ia bergembira dan bersuka cita karena,
melihat perbuatannya sendiri yang bersih.","Here he rejoices, hereafter he rejoices. 
one who performed meritorious deeds rejoices in both existences. 
He rejoices and greatly rejoices when he sees the purity of his own deeds.");
INSERT INTO verse VALUES(17,1,17,17,"Idha tappati pecca tappati, pāpakārī [pāpakāri (?)] ubhayattha tappati;
‘‘Pāpaṃ me kata’’nti tappati, bhiyyo [bhīyo (sī.)] tappati duggatiṃ gato.","Di dunia ini ia menderita.
Di dunia sana ia menderita.
Pelaku kejahatan menderita di kedua dunia itu.
Ia meratap ketika berpikir,
""Aku telah berbuat jahat,"",
dan ia akan lebih menderita lagi,
ketika berada di alam sengsara.","Here he is tormented, hereafter he is tormented. 
the evil-doer is tormented in both existences. 
He is tormented, and he laments: ""Evil have I done."" 
He is even more tormented when he is reborn in one of the lower worlds (Apaya).");
INSERT INTO verse VALUES(18,1,18,18,"Idha nandati pecca nandati, katapuñño ubhayattha nandati;
‘‘Puññaṃ me kata’’nti nandati, bhiyyo nandati suggatiṃ gato.","Di dunia ini ia bahagia.
Di dunia sana ia berbahagia.
Pelaku kebajikan,
berbahagia di kedua dunia itu.
Ia akan berbahagia ketika berpikir,
""Aku telah berbuat bajik"",
dan ia akan lebih berbahagia lagi,
ketika berada di alam bahagia.","Here he is happy, hereafter he is happy. 
one who performs meritorious deeds is happy in both existences. 
Happily he exclaims: I have done meritorious deeds."" 
He is happier still when he is reborn in a higher world (suggati).");
INSERT INTO verse VALUES(19,1,19,19,"Bahumpi ce saṃhita [sahitaṃ (sī. syā. kaṃ. pī.)] bhāsamāno, na takkaro hoti naro pamatto;
Gopova gāvo gaṇayaṃ paresaṃ, na bhāgavā sāmaññassa hoti.","Biarpun seseorang banyak membaca kitab suci,
tetapi tidak berbuat sesuai ajaran,
maka orang lengah itu,
sama seperti gembala sapi yang menghitung sapi milik orang lain.
Ia tak akan memperoleh,
manfaat kehidupan suci.","Though he recites much the Sacred Texts (Tipitaka), 
but is negligent and does not practise according to the Dhamma, 
like a cowherd who counts the cattle of others, 
he has no share in the benefits of the life of a bhikkhu (i.e., Magga-phala).");
INSERT INTO verse VALUES(20,1,20,20,"Appampi ce saṃhita bhāsamāno, dhammassa hoti [hotī (sī. pī.)] anudhammacārī;
Rāgañca dosañca pahāya mohaṃ, sammappajāno suvimuttacitto;
Anupādiyāno idha vā huraṃ vā, sa bhāgavā sāmaññassa hoti.","Biarpun seseorang sedikit membaca kitab suci,
tetapi berbuat sesuai dengan ajaran,
menyingkirkan nafsu indria,
kebencian dan ketidaktahuan,
memiliki pengetahuan benar,
dan batin yang bebas dari nafsu,
tidak melekat pada apapun,
baik di sini maupun di sana.
maka ia akan memperoleh,
manfaat kehidupan suci.","Though he recites only a little of the Sacred Texts (Tipitaka), 
but practises according to the Dhamma, 
eradicating passion, 
ill will and ignorance, 
clearly comprehending the Dhamma, 
with his mind freed from moral defilements and no longer clinging to this world or to the next, 
he shares the benefits of the life of a bhikkhu (i.e., Magga-phala).");
